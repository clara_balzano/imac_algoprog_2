#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{



    indexMin = indexMax = -1;

    int start = 0;
    int end = array.size();

    while (start < end){

        int mid = (start+end)/2;

        if(toSearch>array[mid]){
            start=mid+1;
        }

        else if(toSearch<array[mid]){
            end=mid;
        }

        else{
            indexMin=mid;
            end=mid;
        }
    }


    start=0;
    end=array.size();

    while(start<end){

        int mid=(start+end)/2;

        if(toSearch>array[mid]){
            start=mid+1;
        }

        else if (toSearch<array[mid]){
            end=mid;
        }

        else{
            indexMax=mid;
            start=mid+1;
        }
    }
}


    /*

    Algorithme "naïf"

    indexMin = indexMax = -1;
    int start=0;
    int end=array.size();
    int mid;
    int foundIndex=-1;

    while(start<end){
        mid=(int)(start+end)/2;
        if(toSearch>array[mid]){
            start=mid+1;
        }
        else if(toSearch<array[mid]){
            end=mid;
        }
        else{
            foundIndex=mid;
            break;
        }
    }

    if(foundIndex !=-1){
        int i=0;
        while(array[foundIndex-i]==toSearch){
            indexMin=foundIndex-i;
            i++;
        }


        int j=0;
        while(array[foundIndex+j]==toSearch){
            indexMax=foundIndex+j;
            j++;
        }

    }*/



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 500;
    w = new BinarySearchAllWindow(binarySearchAll);
    w->show();

    return a.exec();
}
