#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int nbNoeud;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
    liste->nbNoeud = 0;
}

bool est_vide(const Liste* liste)
{
    if(liste->nbNoeud == 0){
        return true;
    }
    else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* newNode = new Noeud;
    if(newNode==NULL){
        exit(1);
    }


    if(liste->premier==NULL){
        newNode->donnee=valeur;
        newNode->suivant=NULL;
        liste->premier = newNode;
    }

    else{
        Noeud* tmp = liste->premier;
        while(tmp->suivant){
            tmp=tmp->suivant;
        }
        newNode->donnee=valeur;
        newNode->suivant=NULL;
        tmp->suivant=newNode;


    }
    liste->nbNoeud ++;
}

void affiche(const Liste* liste)
{
    cout<<"Affichage liste : "<<endl;

    if(est_vide(liste)){
        cout<<"La liste est vide."<<endl;
    }
    else{

        Noeud* tmp = liste->premier;

        while(tmp!=NULL){
            cout<<tmp->donnee<<endl;
            tmp=tmp->suivant;
        }
        cout << endl;
    }
}

int recupere(const Liste* liste, int n)
{
    if(n>=0 && n<=liste->nbNoeud){
        Noeud* tmp = liste->premier;

        for(int i=0; i<n; i++){
            tmp = tmp->suivant;
        }

        return tmp->donnee;
    }

    return -1;

}

int cherche(const Liste* liste, int valeur)
{

    Noeud* tmp = liste->premier;

    for(int i=0; i<liste->nbNoeud;i++){
        if(tmp->donnee == valeur){
            return i;
        }
        else{
            tmp=tmp->suivant;

        }
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    if(n>liste->nbNoeud){
        cout<<"Erreur - entrer un indice plus petit"<<endl;
        exit(1);
    }
    Noeud* tmp=liste->premier;
    for(int i=0; i<n;i++){
        tmp = tmp->suivant;
    }
    tmp->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->taille == tableau->capacite)
    {
        tableau->capacite++;
        int* tabDonnees=(int*)realloc(tableau->donnees,tableau->capacite*sizeof(int));
        if(tableau == NULL)
        {
            cout<<"Pb memoire"<<endl;
            exit(1);
        }

        else
        {
            tableau->donnees = tabDonnees;
            tableau->donnees[tableau->taille]=valeur;
            tableau->taille++;
        }
    }

    else
    {
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille++;
    }

}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->taille = 0;
    tableau->capacite=capacite;
    tableau->donnees=(int*)malloc(capacite*sizeof(int));
    if (tableau==NULL)
    {
        cout<<"Pb de memoire"<<endl;
        exit(1);
    }
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->taille==0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    cout<<"Affichage tableau : "<<endl;
    for(int i=0; i<tableau->taille; i++){
        cout<<tableau->donnees[i]<< endl;
    }

}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>tableau->taille){
        cout<<"Erreur - entrer un indice plus petit"<<endl;
        return -1;
    }
    else{
        return tableau->donnees[n];
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int count = 0;
    int index= -1;
    bool trouve = false;

    while(!trouve && count<tableau->taille){
        if(tableau->donnees[count]==valeur){
            index=count;
            trouve = true;
        }
        count++;
    }

    return index;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{

    if(n>tableau->taille){
        cout<<"Erreur - entrer un rang plus petit"<<endl;
    }
    else{
        tableau->donnees[n]=valeur;
    }

}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud* tmp=liste->premier;
    int valeur=tmp->donnee;

    liste->premier=liste->premier->suivant;
    liste->nbNoeud--;
    free(tmp);

    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * newNode = new Noeud;

    newNode->donnee = valeur;
    newNode->suivant = liste->premier;
    liste->premier = newNode;
    liste->nbNoeud++;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud * tmp = liste->premier;
    int valeur = tmp->donnee;
    liste->premier = liste->premier->suivant;
    liste->nbNoeud--;
    free(tmp);

    return valeur;
}



int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
